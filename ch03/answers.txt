1: [Write a code snippet that sets a to an array of n random integers between 0
(inclusive) and n (exclusive).]

import util._    
val n = 100
val a = for (elem <- 0 to n) yield Random.nextInt(n)

2: [Write a loop that swaps adjacent elements of an array of integers. For example,
Array(1, 2, 3, 4, 5) becomes Array(2, 1, 4, 3, 5).]

def swapAdjacent(a: Array[Int]) {
  for (i <- 0 until a.length - 1 if i % 2 == 0) {
    val swap = a(i)
    a(i) = a(i + 1)
    a(i + 1) = swap
  }
}

3: [Repeat the preceding assignment, but produce a new array with the swapped
values. Use for/yield.]

val res = for (i <- 0 until a.length) yield {
  if (i % 2 == 0 && i < a.length - 1)
    a(i + 1)
  else if (i % 2 != 0 && i > 0)
    a(i - 1)
  else
    a(i)
}

4: [Given an array of integers, produce a new array that contains all positive
values of the original array, in their original order, followed by all values that
are zero or negative, in their original order.]

import collection.mutable.ArrayBuffer

val res = ArrayBuffer[Int]()
for (elem <- a if elem > 0) {
  res += elem
}
for (elem <- a if elem <= 0) {
  res += elem
}

5: [How do you compute the average of an Array[Double]?]

var a = Array(1.0, -2.0, 3.0, -4.0, 5.0, 0.0, 5.0, -4.0)

val mean = a.sum / a.size

6: [How do you rearrange the elements of an Array[Int] so that they appear in
reverse sorted order? How do you do the same with an ArrayBuffer[Int]?]

// With Array:
val a = Array(1.0, -2.0, 3.0, -4.0, 5.0, 0.0, 5.0, -4.0)
quickSort(a)
val b = a.reverse

// With ArrayBuffer:
import collection.mutable.ArrayBuffer
var c = ArrayBuffer(1.0, -2.0, 3.0, -4.0, 5.0, 0.0, 5.0, -4.0)
a.sortWith(_>_)

7: [Write a code snippet that produces all values from an array with duplicates
removed. (Hint: Look at Scaladoc.)]

val a = Array(1.0, -2.0, 3.0, -4.0, 5.0, 0.0, 5.0, -4.0, 5.0, 6.0, 5.0)

val b = a.distinct;

8: [Rewrite the example at the end of Section 3.4, “Transforming Arrays,” on
page 32. Collect indexes of the negative elements, reverse the sequence, drop
the last index, and call a.remove(i) for each index. Compare the efﬁciency of
this approach with the two approaches in Section 3.4.]

import collection.mutable.ArrayBuffer

var a = ArrayBuffer(2, -10, 3, -5, 5, -15, 7, 11)

var negIndices = for (i <- 0 until a.length if a(i) < 0) yield {
  i
}
val indices = negIndices.reverse.dropRight(1)
for (elem <- indices)
  a.remove(elem)

9: [Make a collection of all time zones returned by java.util.TimeZone.getAvailableIDs
that are in America. Strip off the "America/" preﬁx and sort the result.]

val timezones = for (elem <- TimeZone.getAvailableIDs() if elem.contains("America")) yield {
  elem.stripPrefix("America/")
}
quickSort(timezones)

10: [Import java.awt.datatransfer._ and make an object of type SystemFlavorMap with
the call
val flavors = SystemFlavorMap.getDefaultFlavorMap().asInstanceOf[SystemFlavorMap]
Then call the getNativesForFlavor method with parameter DataFlavor.imageFlavor
and get the return value as a Scala buffer. (Why this obscure class? It’s hard
to ﬁnd uses of java.util.List in the standard Java library.)]

val flavMap = SystemFlavorMap.getDefaultFlavorMap().asInstanceOf[SystemFlavorMap]
val natives = flavMap.getNativesForFlavor(DataFlavor.imageFlavor)


