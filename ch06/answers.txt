1: [Write an object Conversions with methods inchesToCentimeters, gallonsToLiters, and milesToKilometers.]

object Conversions {
  def inchesToCentimeters(inches: Double) = {
    inches * 2.54
  }
  
  def gallonsToLiters(gallons: Double) = {
    gallons * 4.54609188 
  }
  
  def milesToKilometers(miles: Double) = {
    miles * 1.609344
  }
}

object Main extends App {
  println(Conversions.inchesToCentimeters(12))
  
  println(Conversions.gallonsToLiters(10))
  
  println(Conversions.milesToKilometers(10))
}

2: [The preceding problem wasn’t very object-oriented. Provide a general super-
class UnitConversion and deﬁne objects InchesToCentimeters, GallonsToLiters, and
MilesToKilometers that extend it.]

abstract class UnitConversion {
  def convert(value: Double) : Double
}

object InchesToCentimeters extends UnitConversion {
  def convert(value: Double) = {
    value * 2.54
  }
}

object GallonsToLiters extends UnitConversion {
  def convert(value: Double) = {
    value * 4.54609188 
  }
}
 
object MilesToKilometers extends UnitConversion {
  def convert(value: Double) = {
    value * 1.609344
  } 
}

object Main extends App {
  println(InchesToCentimeters.convert(12))
  
  println(GallonsToLiters.convert(10))
  
  println(MilesToKilometers.convert(10))
}

3: [Deﬁne an Origin object that extends java.awt.Point. Why is this not actually a
good idea? (Have a close look at the methods of the Point class.)]

import java.awt.Point

object Origin extends Point {
}

// It's not a good idea to inherit from point using object, as it's not intended as a singleton type

4: [Deﬁne a Point class with a companion object so that you can construct Point
instances as Point(3, 4), without using new.]

class Point(var x: Int, var y: Int) {
}

object Point {
  def apply(x: Int, y: Int) = new Point(x, y)
}

object Main extends App {
  val point = Point(3, 4)
}

5: [Write a Scala application, using the App trait, that prints the command-line
arguments in reverse order, separated by spaces. For example, scala Reverse
Hello World should print World Hello.]

object Main extends App {
  println(args.reverse.mkString(" "))
}

6: [Write an enumeration describing the four playing card suits so that the toString method returns ♣, ♦, ♥, or ♠.]

object PlayingCardSuites extends Enumeration {
  val Clubs = Value("\u2663")
  val Diamonds = Value("\u2666")
  val Hearts = Value("\u2665")
  val Spades = Value("\u2660")
}

object Main extends App {
  println(PlayingCardSuites.Clubs)
  println(PlayingCardSuites.Spades)
  println(PlayingCardSuites.Diamonds)
  println(PlayingCardSuites.Hearts)
}

7: [Implement a function that checks whether a card suit value from the preceding
exercise is red.]

object PlayingCardSuites extends Enumeration {
  type PlayingCardSuites = Value
  val Clubs = Value("\u2663")
  val Diamonds = Value("\u2666")
  val Hearts = Value("\u2665")
  val Spades = Value("\u2660")
}

import PlayingCardSuites._

object Main extends App { 
  def isRed(suite: PlayingCardSuites) = {
    if (suite == Hearts || suite == Diamonds)
      true
    else
      false 
  }
  
  println(isRed(PlayingCardSuites.Clubs))
  println(isRed(PlayingCardSuites.Spades))
  println(isRed(PlayingCardSuites.Diamonds))
  println(isRed(PlayingCardSuites.Hearts))
}

8: [Write an enumeration describing the eight corners of the RGB color cube. As
IDs, use the color values (for example, 0xff0000 for Red).]

object RgbColorCube extends Enumeration {
  type RgbColorCube = Value
  val Red = Value(Integer.toHexString(java.awt.Color.RED.getRGB))
  val Green = Value(Integer.toHexString(java.awt.Color.GREEN.getRGB))
  val Blue = Value(Integer.toHexString(java.awt.Color.BLUE.getRGB))
  val Cyan = Value(Integer.toHexString(java.awt.Color.CYAN.getRGB))
  val Magenta = Value(Integer.toHexString(java.awt.Color.MAGENTA.getRGB))
  val Yellow = Value(Integer.toHexString(java.awt.Color.YELLOW.getRGB))
  val Black = Value(Integer.toHexString(java.awt.Color.BLACK.getRGB))
  val White = Value(Integer.toHexString(java.awt.Color.WHITE.getRGB))
}

object Main extends App {
  for (color <- RgbColorCube.values) println(color)
}


