1: [Improve the Counter class in Section 5.1, “Simple Classes and Parameterless
MethodsWhy No Multiple Inheritance?,” on page 49 so that it doesn’t turn
negative at Int.MaxValue.]

class Counter {
  private var value = Int.MaxValue - 5
  
  def increment() { 
    if ((value + 1) < Int.MaxValue)
      value += 1
  }
  
  def current = value
}

object Main extends App {
  val counter = new Counter
  counter.increment
  println(counter.current)
  counter.increment
  println(counter.current)
  counter.increment
  println(counter.current)
  counter.increment
  println(counter.current)
  counter.increment
  println(counter.current)

  // ...will never get greater than Int.MaxValue
}

2: [Write a class BankAccount with methods deposit and withdraw, and a read-only
property balance.]

class BankAccount(private var _balance : Double = 0.0) {  
  def deposit(amount : Double) {
    _balance += amount
  }
  
  def withdraw(amount : Double) = {
    if (_balance - amount < 0)
      false
    else {
      _balance -= amount
      true
    }
  }
  
  def balance = _balance
}

3: [Write a class Time with read-only properties hours and minutes and a method
before(other: Time): Boolean that checks whether this time comes before the
other. A Time object should be constructed as new Time(hrs, min), where hrs is in
military time format (between 0 and 23).]

class Time(private var _hours : Int, private var _minutes: Int) {
  def before(other: Time) = {
    if (_hours < other._hours) {
      if (_minutes < other._minutes)
        true
    }
        
    false
  }
  
  def hours = _hours
  
  def minutes = _minutes
  
  if (_hours < 0)
    _hours = 23
  if (_hours > 23)
    _hours = 0
  
  if (_minutes < 0)
    _minutes = 59
  if (_minutes > 59)
    _minutes = 0
}

4: [Reimplement the Time class from the preceding exercise so that the internal
representation is the number of minutes since midnight (between 0 and
24 × 60 – 1).Do not change the public interface. That is, client code should be
unaffected by your change.]

class Time(private var hrs : Int, private var mins: Int) {
  def before(other: Time) = {
    if (_minutes < other._minutes)
      true
    else
      false
  }
  
  def hours = _minutes / 60
  
  def minutes = _minutes % 60
  
  val _totalMinutes = 24 * 60 - 1
  
  var _minutes = (hrs * 60) + mins 
  
  if (_minutes < 0)
    _minutes = _totalMinutes
  else if (_minutes > _totalMinutes)
    _minutes = 0
}

5: [Make a class Student with read-write JavaBeans properties name (of type String)
and id (of type Long). What methods are generated? (Use javap to check.) Can
you call the JavaBeans getters and setters in Scala? Should you?]

class Student(@BeanProperty var name: String, @BeanProperty var id: Long) {  
}

object Main extends App {
  val human = new Student("Dave Grohl", 1337)
  
  // You can call the JavaBeans getter and setter, just like this
  human.setName("Kurt Cobain")
  human.setId(5000)
  
  // But why would you, when the Scala-esque methods are generated?
  human.name = "Krist Novoselic"
  human.id = 5001
}

6: [In the Person class of Section 5.1, “Simple Classes and Parameterless Meth-
odsWhy No Multiple Inheritance?,” on page 49, provide a primary constructor
that turns negative ages to 0.]

class Person(private var _age: Int) {
  if (_age < 0)
    _age = 0
    
  def age = _age
}

object Main extends App {
  val person = new Person(50)
  
  println(person.age)
  
  val person2 = new Person(-50)
  
  println(person2.age)
}

7: [Write a class Person with a primary constructor that accepts a string containing
a ﬁrst name, a space, and a last name, such as new Person("Fred Smith"). Supply
read-only properties firstName and lastName. Should the primary constructor
parameter be a var, a val, or a plain parameter? Why?]

// Primary constructor parameter should be a val: immutable, but still with public getter so user can get the full name of the person
class Person(val name: String) {
  val firstName = name.split(" ")(0)
  
  val lastName = name.split(" ")(1)
}

object Main extends App {
  val dave = new Person("Dave Grohl")
  
  println(dave.name)
  println(dave.firstName)
  println(dave.lastName)
}

8: [Make a class Car with read-only properties for manufacturer, model name,
and model year, and a read-write property for the license plate. Supply four
constructors. All require the manufacturer and model name. Optionally,
model year and license plate can also be speciﬁed in the constructor. If not,
the model year is set to -1 and the license plate to the empty string. Which
constructor are you choosing as the primary constructor? Why?]

// Using primary constructor only, as it offers behaviour we need: default parameters
class Car(val manufacturer: String, 
    val modelName: String, 
    val modelYear: Int = -1, 
    var licensePlate: String = "") {
}

object Main extends App {
  val aston = new Car("Aston Martin", "DB9", 2009, "007")
  
  println(aston.manufacturer)
  println(aston.modelName)
  println(aston.modelYear)
  println(aston.licensePlate)
  
  val reliant = new Car("Robin", "Reliant")
  
  println(reliant.manufacturer)
  println(reliant.modelName)
  println(reliant.modelYear)
  println(reliant.licensePlate)
}

9: [Reimplement the class of the preceding exercise in Java, C#, or C++ (your
choice). How much shorter is the Scala class?]

class Car {
    public:
        Car(const std::string& manufacturer,
            const std::string& modelName,
            const int modelYear = -1,
            const std::string& licensePlate = "") 
          : _manufacturer(manufacturer),
            _modelName(modelName), 
            _modelYear(modelYear),
            _licensePlate(licensePlate) {
        }

        std::string manufacturer() const {
            return _manufacturer;
        }

        std::string modelName() const {
            return _modelName;
        }

        int modelYear() const {
            return _modelYear;
        }

        std::string licensePlate() const {
            return _licensePlate;
        }

        void setLicensePlate(const std::string& licensePlate) {
            _licensePlate = licensePlate;
        }

    private:
	std::string manufacturer;
        string modelName;
        int modelYear;
        std::string licensePlate;
}

// 35 lines C++ vs 4 lines for Scala (technically 1). So lots shorter.

10: [Consider the class
class Employee(val name: String, var salary: Double) {
  def this() { this("John Q. Public", 0.0) }
}
Rewrite it to use explicit ﬁelds and a default primary constructor. Which form
do you prefer? Why?]

class Employee(_name: String = "John Q. Public", _salary: Double = 0.0) {
  val name = _name;
  var salary = _salary;
}

// I prefer the above relative to using an auxiliary constructor. In fact, I prefer the use of a default primary constructor, and no explit fields, like this, due to brevity.

class Employee(val name: String = "John Q. Public", var salary: Double = 0.0) {
}

